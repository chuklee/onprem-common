package common

import (
	"fmt"
	"os"
	"time"

	"encoding/json"
	"io/ioutil"
)

const (
	COUNT        = "1a"
	STATE        = "2a"
	ANALOG_VALUE = "3a"
	AUK_HOME     = "AUK_HOME"
	AUK_CONFIG   = "auk_config.json"
	MAX_CHANNELS = 6
)

type Reading struct {
	ClientID  string                 `bson:"clientid"`
	Username  string                 `bson:"username,omitempty"`
	Topic     string                 `bson:"topic"`
	Data      map[string]interface{} `bson:"data"`
	Timestamp time.Time              `bson:"ts"`
}

type AukAdmin struct {
	Username   string `json:"username"`
	Password   string `json:"password"`
	EnableCors bool   `json:"enableCors"`
	Port       int    `json:"port"`
	Release    bool   `json:"release"`
	JWTSecret  string `json:"jwtSecret"`
}

type AukConfig struct {
	MongoUrl            string   `json:"mongoUrl"`
	Database            string   `json:"database"`
	DataCollection      string   `json:"data"`
	ReferenceCollection string   `json:"reference"`
	Admin               AukAdmin `json:"admin,omitempty"`
}

func ReadConfig() (AukConfig, error) {

	var aukConfig AukConfig
	aukConfig = DefaultConfig()

	configPath, ok := os.LookupEnv(AUK_HOME)
	if !ok {
		configPath = "."
	}

	envConfig := fmt.Sprintf("%s/%s", configPath, AUK_CONFIG)

	configFile, err := ioutil.ReadFile(envConfig)
	if nil != err {
		return aukConfig,
			fmt.Errorf("Cannot find auk_config.json. Using default config: %v", err)
	} else if err := json.Unmarshal(configFile, &aukConfig); nil != err {
		return aukConfig,
			fmt.Errorf("Cannot parse auk_config.json. Using default config: %v", err)
	}
	return aukConfig, nil
}

func DefaultConfig() AukConfig {
	admin := AukAdmin{
		Username:   "admin",
		Password:   "YWRtaW4=",
		EnableCors: false,
		Port:       8080,
		Release:    true,
		JWTSecret:  "c2hoLi4u",
	}
	return AukConfig{
		MongoUrl:            "mongodb://localhost:27017",
		Database:            "auk",
		DataCollection:      "readings",
		ReferenceCollection: "references",
		Admin:               admin,
	}
}
