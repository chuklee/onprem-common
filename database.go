package common

import (
	//"encoding/json"
	//"math/big"

	ctx "context"

	//"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type OnPremDatabase struct {
	Config AukConfig
	client *mongo.Client
}

func (db *OnPremDatabase) Connect() error {
	opts := options.Client().ApplyURI(db.Config.MongoUrl)
	client, err := mongo.NewClient(opts)
	if nil != err {
		return err
	}
	if err := client.Connect(ctx.TODO()); nil != err {
		return err
	}
	db.client = client
	return nil
}

func (db *OnPremDatabase) Disconnect() error {
	return db.client.Disconnect(ctx.TODO())
}

func (db *OnPremDatabase) GetDataCollection(d *mongo.Database) *mongo.Collection {
	return db.GetCollection(db.Config.DataCollection, d)
}
func (db *OnPremDatabase) GetReferenceCollection(d *mongo.Database) *mongo.Collection {
	return db.GetCollection(db.Config.ReferenceCollection, d)
}
func (db *OnPremDatabase) GetCollection(c string, d *mongo.Database) *mongo.Collection {
	return d.Collection(c)
}

func (db *OnPremDatabase) GetDatabase() *mongo.Database {
	return db.client.Database(db.Config.Database)
}
